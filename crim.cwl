cwlVersion: v1.0
class: CommandLineTool
baseCommand: s1_coherence_cd
requirements:
  ResourceRequirement:
    ramMin: 24000
hints:
  DockerRequirement:
    dockerPull: obarrilero/s1coherence:1.0
id: satcen-coherence-crim
label: Satcen S1 Coherence Process (CRIM version)
doc: The application computes the coherence of two Sentinel-1 IW products. The output is a RGB (master backscatter, slave backscatter, coherence) in GeoTiff format.
inputs:
  input_files:
    inputBinding:
      position: 1
      prefix: --input_files
    type:
      items: File
      type: array
  aoi_wkt:
    inputBinding:
      position: 2
      prefix: --aoi_wkt
    type: string?
outputs:
  output:
    outputBinding:
      glob: '*.tif'
    type: File
  metadata:
    outputBinding:
      glob: 'metadata.xml'
    type: File

